# -*- coding: utf-8 -*-

if 0: # Stuff for Eclipse
    from gluon.dal import DAL
    from gluon.tools import Auth, Service, Crud
    from gluon.sqlhtml import URL
    from gluon.http import redirect
    from gluon.languages import translator as T

    db = DAL()
    auth = Auth()
    service = Service()
    crud = Crud()
    
    global db
    global request
    global session
    global response
    from gluon.globals import Request, Session, Response
    
    req = Request()
    req = request
    ses = Session()
    ses = session
    resp = Response()
    resp = response

def index():
    pass

def d():
    group = db(db.picnics.group_name == request.args(0)).select() or redirect(URL('groups', ' ')) 
    events = db(db.event.group_id == group[0].id).select()
    if request.args(1):
        requested_event = None
        try:
            if request.args(1) in ['join', 'leave']:
                return update_group_membership(leave_or_join=request.args(1), group_id=group[0].id, user_id=auth.user.id, goto=request.args(0))
            else:
                for event in events.find(lambda event: event.id == int(request.args(1))):
                    requested_event = event
                    break
                if request.args(2) == 'rsvp':
                    if request.args(3) in ['yes', 'maybe', 'no']:
                        update_rsvp(rsvp=request.args(3), event_id=requested_event.id, user_id=auth.user.id) 
        except ValueError:
            response.flash = T('Couldn\'t find specified event')
            return redirect(URL('', 'group\d\\' + request.args(0), ' '))
        return event_page(requested_event, group[0].group_name)
    return dict(group=group, events=events)

def event_page(requested_event, group_name):
    response.view = 'group/event_page.html'
    eventpage = requested_event or redirect(URL('', 'group/d/' + str(group_name)))

    return dict(group_name=group_name, eventpage=eventpage)

@auth.requires_login()
def update_rsvp(rsvp, event_id, user_id):
    db.rsvp_list.update_or_insert((db.rsvp_list.user_id == user_id) & (db.rsvp_list.event_id == event_id),
                                  rsvp=rsvp, event_id=event_id, user_id=user_id)

@auth.requires_login()
def update_group_membership(leave_or_join, group_id, user_id, goto):
    if leave_or_join == "leave":
        db((db.group_member_list.user_id == user_id) & (db.group_member_list.group_id == group_id)).delete()
    else:
        db.group_member_list.update_or_insert((db.group_member_list.user_id == user_id)
                                              & (db.group_member_list.group_id == group_id),
                                              group_id=group_id, user_id=user_id)
    return redirect(URL('/d/' + goto))

